@extends('templates.template')
@section('title','Catalog')
@section('content')


<h1 class="text-center py-5">CAtalog</h1>

<div class="container">
	<div class="row">
		@foreach($items as $item)
		<div class="col-lg-4">
			<div class="card">
				<img class="card-img-top" src="{{asset($item->img_path)}}">
				<div class="card-body">
					<h5 class="card-title"></h5>
					<p class="card-text">{{$item->name}}</p>
					<p class="card-text">{{$item->description}}</p>
					<p class="card-text">{{$item->price}}</p>
					<p class="card-text">{{$item->category_id}}</p>
					<p class="card-text">{{$item->category_id}}</p>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>

@endsection